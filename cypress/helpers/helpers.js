export class Helpers {

    static upload(uploadfile) {
        cy.contains("Upload").click()
        cy.visit('/upload/finalize')
        cy.get('input[type="file"]').attachFile(uploadfile, {fileName: 'simpson.gif'})
        cy.get("button.sc-kgAjT.cIeFYm").click()
    }

    static delete(){
        cy.get("._3VnuN1NZurY9lA0b_kRnUu._2-lZy94jww5Hv-2mFzCZA1").click({force:true})
        cy.get('._3mq9PoAndlAVwPnNxUbDGf.ss-trash').click({force:true})
        cy.get("._2Ga8Um_-9h1PxOK6XrxyxP > :nth-child(2)").click({force:true})
    }

    static search(text) {
        cy.get(".Input-sc-w75cdz").type(`${text} {enter}`)
    }
    static edit(tag){
        cy.get("._3VnuN1NZurY9lA0b_kRnUu._2-lZy94jww5Hv-2mFzCZA1").click({force:true})
        cy.xpath('(//*[@class="Fx76GcH4nzz12sVCkdN1_ _2-GxqWYNxZ-IXQRDqN-Soq _1bc7yOoPoF9ZAns5laPqC0"])[2]').type(tag)
        cy.xpath("(//*[@class='-_MtFcxtXIz-j7ZjIaz7X _1Fba10Vcpc4_UBtLy_oMYy'])[2]").click({force:true})
    }
}


