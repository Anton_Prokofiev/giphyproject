/// <reference types="Cypress"/>

describe("Giphy API Tests", () => {

    it("API test- 401 Unauthorized error", () => {

        cy.request({
            method: "GET",
            url: 'api.giphy.com/v1/gifs/search?gif_id=h0m6DAVQ1zBfi',
            failOnStatusCode: false
        }).then((response) => {
            expect(response.status).to.eq(401);
            expect(response.body).to.have.property('message', 'No API key found in request');
        })
    })

    it("API test- 200 Success status ", () => {

        cy.fixture("apiConfig").as('config');
        cy.get('@config').then(config => {
            let urlSearch = config.urlSearch;
            let api_key = config.api_key;
            cy.request({
                method: "GET",
                url: `${urlSearch}api_key=${api_key}&gif_id=h0m6DAVQ1zBfi`
            }).then((response) => {
                expect(response.status).to.eq(200);
                expect(response.body.meta).to.have.property('msg', 'OK');
            })
        })
    })
    it("API test- Upload gif ", () => {

        cy.fixture("apiConfig").as('config');

        cy.get('@config').then(config => {
            let url = config.urlUpload;
            let api_key = config.api_key;
            let imageUrl = config.imageUrl

            cy.uploadFile(url, api_key, imageUrl).then((response) => {
                expect(response.status).to.eq(200);
                expect(response.body.meta).to.have.property('msg', 'OK');
            })
        })
    })
})