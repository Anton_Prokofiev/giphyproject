/// <reference types="Cypress"/>

import { Helpers } from '../../helpers/helpers'


describe("Testing giphy application", () => {

    before(() => {
        cy.login(Cypress.env('USEREMAIL'), Cypress.env('USERPASSWORD'))
    })
    it.only("Verify that user has ability to add 'Tag' to a gif via edit functionality", () => {
        //expect(Cypress.env('foo')).to.be.a('string')
       // expect(Cypress.env('USERPASSWORD')).to.be.a('string')
        Helpers.upload('testgif.gif')
        Helpers.edit('somerandomtag')
        cy.get('._10zmt0MZBz5vglK8GXKdMz').should('contain', 'somerandomtag')
        Helpers.delete()
    })
})