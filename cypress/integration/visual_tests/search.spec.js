/// <reference types="Cypress"/>

import { Helpers } from '../../helpers/helpers'


describe("Testing giphy application", () => {

    before(() => {
        cy.login(Cypress.env('USEREMAIL'), Cypress.env('USERPASSWORD'))
    })
    it("Verify that correct search result is displayed", () => {
        cy.visit('/');
        Helpers.search("cats")
        cy.get('h1').should("contain", 'cats')
        cy.url().should("contain", 'cats')
    })
})