/// <reference types="Cypress"/>

import { Helpers } from '../../helpers/helpers'


describe("Testing giphy application", () => {

    before(() => {
        cy.login(Cypress.env('USEREMAIL'), Cypress.env('USERPASSWORD'))
    })

    it("Verify gif upload/delete functionality", () => {
        Helpers.upload('testgif.gif')
        cy.get("img[alt='Animated GIF']").should("be.visible")
        cy.get('.KRS9L9BsuEdhF-ACKiX8x > div img').invoke('attr', 'src').then(gifLink => {
            Helpers.delete()
            cy.xpath("(//*[@class='Content-sc-1lsonyy gqzsba'])[2]").should("not.contain", gifLink)
        })
    })
})