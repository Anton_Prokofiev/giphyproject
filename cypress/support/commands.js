// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
import 'cypress-file-upload';

Cypress.Commands.add('login', (useremail, password) => {
    cy.visit("/login")
    cy.get('[name="email"]', { log: false }).type(useremail, { log: false })
    cy.get('[name="password"]', { log: false }).type(password, { log: false })
    cy.contains('Log in').click({force:true})
})

Cypress.Commands.add('uploadFile', (url, api_key, imageUrl) => {
    cy.request({
        method: "POST",
        url: url,
        form: true,
        body: {
            api_key: api_key,
            source_image_url: imageUrl
        }
    })
    return cy;
})
